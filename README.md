# ===== DEPENDENCY =====
In programming, dependency means that one class uses another class.
Example: 
If Class A needs an object in Class B, then Class A depends on Class B, while Class B becomes a dependency of Class A.

# ===== DEPENDENCY INJECTION =====
Injection simply means passing the dependencies from the outside.
The classes should not be responsible for creating their own dependencies or searching for them.
These dependencies should be instantiated somewhere else and passed to the class that needs them.
By using dependency injection, the other classes can be worked in parallel withour interfering with the work on the current class.
Simply get needed objects passed when they are ready, to make the current class with less responsibility.
We can also passes the same object for other different classes.

# ===== DAGGER 2 =====
Dagger 2 is a dependency injection framework for Android and Java, also Kotlin.
Dagger 2 is developed by Google.
Dagger 2 helps solving manual DI problems and get rid of all the boilerplate (code that must be inputted into many places with minor changes) code.
Dagger will construct all depended objects and Dagger creates them at the right time and in the right order.

# Component (also called as 'The Injector')
Component is the most important piece of Dagger.
The component creates and stored our objects and then provides them to us.
The component knows the needed object for the class and it also knows about any other necessary dependencies further down the road.
The component also knows in what order it has to create them without having to specify it explicitly.
The component creates a dependency graph called "Directed Arcyclic Graph" (DAG)	=> DAGGER
Directed	=> It has arrows that go into one direction.
Aryclic		=> There are no cycles, nothing is a child of itself.

Two ways in which the component can provide the dependencies:
1. The component inject them into the member variable of the class directly
2. Provision method, basically a getter method

~ Annotation '@Component' in the Component Interface will make Dagger implement the interface and create all necessary code at the compile time. (Don't have to do anything else and don't have to specify how the methods work. Dagger does all of it).
~ Annotation '@Inject' is used to instantiate the object, to mark all the constructors that it is supposed to use. Dagger will basically knows that this is the way it has to instantiate the object.

In the activity, we need an instance of the component. The 'Dagger'+'Interface Nama' will be created if the compile has been done. 